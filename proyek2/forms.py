from django import forms
from .models import *
class DateTimeInput(forms.DateTimeInput):
    input_type = 'datetime-local'
class Schedule_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    kegiatan = forms.CharField(label = 'Nama Kegiatan', required = True, widget=forms.TextInput(attrs=attrs), max_length=20)
    waktu = forms.DateTimeField(label = 'Waktu Kegiatan', required = True, widget=DateTimeInput(attrs=attrs), input_formats = '%m/%d/%Y %H:%M')
    tempat = forms.CharField(label = 'Tempat Kegiatan', required = True, widget=forms.TextInput(attrs=attrs), max_length=20)
    kategori = forms.CharField(label = 'Kategori', required = False, widget=forms.TextInput(attrs=attrs), max_length=20)

    class Meta:
        model = JadwalPribadi
        
