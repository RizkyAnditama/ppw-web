from django.urls import re_path, path
from .views import index,eduexp,skilltrait,contacts,registration, schedules, schedule_post
#url for app
urlpatterns = [
    re_path(r'home/', index, name='index'),
    re_path(r'education-and-experience/', eduexp, name='eduexp'),
    re_path(r'skill-and-traits/', skilltrait, name='skilltrait'),
    re_path(r'contacts/', contacts, name='contacts'),
    re_path(r'registration/', registration, name='registration'),
    re_path(r'schedules/', schedules, name='schedules'),
    re_path(r'post_schedule/', schedule_post, name='schedule_post'),
]
