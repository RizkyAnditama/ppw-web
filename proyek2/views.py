from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import *
from .models import JadwalPribadi
response = {'author': "Andi"}
def index(request):
    return render(request, 'home.html', {})
def eduexp(request):
    return render(request, 'eduexp.html', {})
def skilltrait(request):
    return render(request, 'skilltrait.html', {})
def contacts(request):
    return render(request, 'contacts.html', {})
def registration(request):
    return render(request, 'registration.html', {})
def schedules(request):
    response['schedule_form'] = Schedule_Form
    return render(request, 'schedules.html', response)
def schedule_post(request):
    form = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and 'submit' in request.POST):
        response['kegiatan'] = request.POST['kegiatan']if request.POST['kegiatan']!="" else "Anonymous"
        response['waktu'] = request.POST['waktu']if request.POST['waktu']!="" else "Anonymous"
        response['tempat'] = request.POST['tempat']if request.POST['tempat']!="" else "Anonymous"
        response['kategori'] = request.POST['kategori']
        jadwalpribadi = JadwalPribadi(kegiatan=response['kegiatan'], waktu=response['waktu'], tempat=response['tempat'], kategori=response['kategori'])
        jadwalpribadi.save()
        semuajadwal = JadwalPribadi.objects.all()
        response['schedule'] = semuajadwal
        html = 'schedules_result.html'
        return render(request, html, response)
    elif (request.method == 'POST' and 'delete' in request.POST):
        html = 'schedules_result.html'
        semuajadwal = JadwalPribadi.objects.all()
        response['schedule'] = semuajadwal.delete()
        return render(request,html, response)
    else:
        return HttpResponseRedirect('/schedules/')


# Create your views here.
